|
|:-:
|![choptank-ui](https://bitbucket.org/repo/84nrGA/images/2741158619-choptank-ui--banner.png)   
|[![Release Version](https://img.shields.io/badge/release-1.0.0-blue.svg?style=flat-square)](https://bitbucket.org/bmoyer240/choptank-ui-sublime-text/src/e14bb9df116621dfd4cf0aac7441d0d83b3c09c3?at=1.0.0) ![Downloaded](https://img.shields.io/badge/downloads-97-lightgrey.svg?style=flat-square)


A simple dark interface with a matching syntax theme for **Sublime Text 3 v3116+** Release 1.0.0 is only the start
of the choptank-ui project. Releases for **Atom.io** and **VS Code** are on the horizon so please, stay tuned.[Link Text](Link URL)  
  
  
***
***
  

**Main Window**
![main window](https://bitbucket.org/repo/84nrGA/images/197655119-screen-main.png)
   
   
   

**Package Control**
![screen-packagecontrol.png](https://bitbucket.org/repo/84nrGA/images/323204807-screen-packagecontrol.png)
   
   
   
   

**Console**
![screen-console.png](https://bitbucket.org/repo/84nrGA/images/1896592196-screen-console.png)